# Compute CIAIR, MIT_KEMAR and SCUT_KEMAR automatically using this separate script.

# Run makemhr command in a different directory.
from subprocess import run

# Create directories.
from os import mkdir

# Determine if a file exists or not.
from pathlib import Path

# Unzip downloaded MIT_KEMAR full.zip file.
from zipfile import ZipFile

# Extract downloaded CIAIR data02.tgz file.
import tarfile

# Download .zip, .tgz and .sofa files.
import urllib.request as request

rate = "44100"
dataset = "sphere"
threads = "6"

mhr_dir = "misc"
ext_dir = "uncompiled"
zip_dir = "downloaded"

if not Path(mhr_dir).is_dir():
    mkdir(mhr_dir)
if not Path(ext_dir).is_dir():
    mkdir(ext_dir)
if not Path(zip_dir).is_dir():
    mkdir(zip_dir)

# archive.org website:
# https://web.archive.org/web/20150805091853/http://www.sp.m.is.nagoya-u.ac.jp/HRTF/archive/data02.tgz
# Please note that downloading a file from the above will return the page HTML only.
# Use the direct download link below.
# https://web.archive.org/web/20150805091853if_/http://www.sp.m.is.nagoya-u.ac.jp/HRTF/archive/data02.tgz
sources = {
    "CIAIR": "https://web.archive.org/web/20150805091853if_/http://www.sp.m.is.nagoya-u.ac.jp/HRTF/archive/data02.tgz",
    "MIT_KEMAR": "https://sound.media.mit.edu/resources/KEMAR/full.zip",
    "MIT_KEMAR_sofa": "https://sofacoustics.org/data/database/mit/mit_kemar_normal_pinna.sofa",
    "SCUT_KEMAR": "https://sofacoustics.org/data/database/scut/SCUT_KEMAR_radius_all.sofa",
}


def download_source(fd, URL):
    if Path(fd).is_file():
        return print(f"{fd} already downloaded; skipping")

    print(f"Downloading file {fd}")
    download = request.urlopen(URL)

    with open(fd, "wb") as file:
        file.write(download.read())

    print(f"Download complete: {fd}")


# Generic tar extraction for CIAIR.
def extract_tar(fd):
    print(f"Extracting tarfile {fd}")
    tar = tarfile.open(fd, "r")
    for item in tar:
        tar.extract(item, ext_dir)

    print(f"Extraction {fd} complete!")


# Extract MIT zip specifically into folder "MITfull" and excluding unused directory "headphones+spkr".
def extract_MIT(fd):
    print(f"Extracting MIT {fd}")
    archive = ZipFile(fd)

    for file in archive.namelist():
        if not file.startswith("headphones+spkr"):
            archive.extract(file, f"{ext_dir}/MITfull")
    print(f"Extraction MIT {fd} complete!")


# Compile non-sofa extracted files.
def compile_files(s):
    # Paths relative to ext_dir
    def_path = f"../def/{s}.def"
    compile_path = f"../{mhr_dir}/{s}_{rate}.mhr"

    print(f"Compiling {s}")

    # Change directory to ext_dir before running command, since .def file requires specific CWD.
    run(
        [
            "makemhr",
            "-r",
            rate,
            "-d",
            dataset,
            "-i",
            def_path,
            "-o",
            compile_path,
            "-j",
            threads,
        ],
        cwd=ext_dir,
    )

    print(f"--> Compilation {s} complete!")


def compile_sofa(s, fn):
    # Paths relative to zip_dir
    def_path = f"../def/{s}.def"
    compile_path = f"../{mhr_dir}/{s}_{rate}.mhr"

    print(f"Compiling SOFA {s}")
    # Work from zip_dir since .sofa files do not need to be moved.
    run(
        [
            "makemhr",
            "-r",
            rate,
            "-d",
            dataset,
            "-i",
            def_path,
            "-o",
            compile_path,
            "-j",
            threads,
        ],
        cwd=zip_dir,
    )

    print(f"---> Sofa compilation {s} complete!")


# source is the name of the dataset.
# sources[source] is the download URL.
for source in sources:
    file_url = sources[source]
    file_name = file_url.split("/")[-1]
    file_directory = f"{zip_dir}/{file_name}"

    if Path(file_directory).is_file():
        print(f"File {file_directory} already exists!")
    else:
        download_source(file_directory, file_url)

    # Extract CIAIR using tarfile
    if file_name.endswith(".tgz"):
        extract_tar(file_directory)
    # Extract MIT_KEMAR using zipfile ZipFile
    if file_name.endswith(".zip"):
        extract_MIT(file_directory)

    # .sofa do not need to be extracted, compile immediately.
    # Provide file name, as datasets do not match their .sofa file.
    if file_name.endswith(".sofa"):
        compile_sofa(source, file_name)
    else:
        compile_files(source)
