# MakeIRCAM

Python script to run makemhr on each sample at http://recherche.ircam.fr/equipes/salles/listen/download.html

# Dependencies
Packages required in an Arch Linux VM to update this repository whenever necessary:
 - dhcpcd (internet connectivity)
 - git
 - openssh (ssh-keygen)
 - openal (includes makemhr)
 - libmysofa (required library for makemhr)

# Other notes
`mount -o remount,size=8G /run/archiso/cowspace` is useful for increasing temporary storage when running live ISO Arch with 8GB RAM and not wanting git to crash.

Originally made per some guide on System Shock 2 HRTF fixing where the game uses 22050hz sound files so these are compiled for 44100hz instead and then upsampled to 48000hz in alsoft-config. Revisiting this project for Penumbra: Overture which also uses OpenAL from its included Creative installer, though am unsure what hz its soundfiles are yet.

MIT KEMAR sofa or not produce the exact same file.

This repository is super slow to download as I'm not bothered setting up LFS. Sorry.
