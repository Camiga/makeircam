# Run makemhr command and create directories.
import os

# Deleting a non-empty directory.
import shutil

# Determine if a file exists or not.
from pathlib import Path

# Unzip downloaded IR_10XX.zip files.
from zipfile import ZipFile

# Download IR_10XX.zip files.
import urllib.request as request

rate = "44100"
dataset = "sphere"
threads = "6"

mhr_dir = "IRCAM"
zip_dir = "downloaded"

if not Path(mhr_dir).is_dir():
    os.mkdir(mhr_dir)
if not Path(zip_dir).is_dir():
    os.mkdir(zip_dir)

samples = [
    "02",
    "03",
    "04",
    "05",
    "06",
    "07",
    "08",
    "09",
    "12",
    "13",
    "14",
    "15",
    "16",
    "17",
    "18",
    "20",
    "21",
    "22",
    "23",
    "25",
    "26",
    "28",
    "29",
    "30",
    "31",
    "32",
    "33",
    "34",
    "37",
    "38",
    "39",
    "40",
    "41",
    "42",
    "43",
    "44",
    "45",
    "46",
    "47",
    "48",
    "49",
    "50",
    "51",
    "52",
    "53",
    "54",
    "55",
    "56",
    "57",
    "58",
    "59",
]

small_samples = ["02", "03"]


def download_sample(s):
    download_directory = f"{zip_dir}/IRC_10{s}.zip"

    # Ensure file doesn't already exist.
    if Path(download_directory).is_file():
        return print(f"{download_directory} already downloaded; skipping")

    # Make a request to IRCAM to download the sample required.
    print(f"Downloading file {download_directory}")
    download = request.urlopen(
        f"ftp://ftp.ircam.fr/pub/IRCAM/equipes/salles/listen/archive/SUBJECTS/IRC_10{s}.zip"
    )

    # Write downloaded file to directory in project.
    with open(download_directory, "wb") as file:
        file.write(download.read())

    print(f"Download complete: {download_directory}")


def extract_sample(s):
    extract_directory = f"IRC"

    # Since directory will be deleted once compiled, no checking is required.

    print(f"Extracting to {extract_directory}")
    # Open zip file from download_sample().
    archive = ZipFile(f"{zip_dir}/IRC_10{s}.zip")
    # Only extract files from the WAV directory inside the RAW directory.
    for file in archive.namelist():
        if file.startswith("RAW/WAV/"):
            archive.extract(file, extract_directory)

    print(f"Extraction {extract_directory} complete.")


def define_sample(s):
    define_path = f"IRC/IRC.def"

    if Path(define_path).is_file():
        return print(f"{define_path} already written; skipping")

    print(f"Creating {define_path}")
    # Read default IRC_1005 definition file into the data variable, replacing
    # 1005 with your specified sample.
    with open("def/IRC_1005.def", "r") as file:
        data = file.read().replace("IRC_1005", f"IRC_10{s}")
    # Write the edited template file to the new sample directory.
    with open(define_path, "w") as file:
        file.write(data)

    print(f"{define_path} created successfully.")


def compile_sample(s):
    compile_path = f"{mhr_dir}/IRC_10{s}_{rate}.mhr"

    if Path(compile_path).is_file():
        return print(f"{compile_path} already compiled; skipping")

    print(f"Compiling {compile_path}")

    # Run operating system command on .def file.
    os.system(
        f"makemhr -r {rate} -d {dataset} -i IRC/IRC.def -o {compile_path} -j {threads}"
    )

    # Remove extracted files, original zips will remain intact.
    shutil.rmtree(f"IRC")

    print(f"Build {compile_path} complete!")


# Replace small_samples to download all 51 items.
for sample in samples:
    # Don't bother with sample if its .mhr exists.
    if Path(f"{mhr_dir}/IRC_10{sample}_{rate}.mhr").is_file():
        print(f"Sample IRC_10{sample} already exists!")
    else:
        download_sample(sample)
        extract_sample(sample)
        define_sample(sample)
        compile_sample(sample)
